package com.gaialab.RedisTest

import com.redis._
import scalaz.effect.IO

/**
  * Created by ktz on 16. 1. 22.
  */
trait RedisClientWrapper {
  def GetClient(host : String = "localhost", port : Int = 6379, password : String) : RedisClient = new RedisClient(host, port, secret = if(password.isEmpty) None else Some(password))

  def SetKey(redisClient : RedisClient, key : String) : IO[Boolean] = IO{
    redisClient.hset(key, "ejabberd@localhost", "asdadoijqoiwdjqowidjoiqjdoiqwjdoisajsjfudguyqwguyqdguyagscjsbcjbsciugwqhidhwqiudhqiudhiuhqiudwhiuwdhqiudhiuwqasdadoijqoiwdjqowidjoiqjdoiqwjdoisajsjfudguyqwguyqdguyagscjsbcjbsciugwqhidhwqiudhqiudhiuhqiudwhiuwdhqiudhiuwq")
  }

  def GetKey(redisClient: RedisClient, key : String) : IO[Unit] = IO{
    redisClient.hget(key, "ejabberd@localhost")
  }

  def DeleteKey(redisClient: RedisClient, key : String) : IO[Unit] = IO{
    redisClient.del(key)
  }
}
