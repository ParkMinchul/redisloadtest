package com.gaialab.RedisTest

import akka.actor.{Props, ActorSystem}
import com.typesafe.config.ConfigFactory

import scala.annotation.tailrec

/**
  * Created by ktz on 16. 1. 22.
  */
object Main extends App{
  val system = ActorSystem("RedisLoadClient")
  val monitor = system.actorOf(Props[MonitoringActor], "MonitorActor")
  val configFactory = ConfigFactory.load("application")
  val redisworker = system.actorOf(Props(new RedisClientPoolActor(configFactory.getString("redis.host"), configFactory.getInt("redis.port"),configFactory.getString("redis.password"), configFactory.getInt("redis.nOfKeys"), monitoringActor = monitor)), "redisPool")

  Menu(0)

  @tailrec
  def Menu(choice : Int) : Unit = {
    choice match {
      case 1 => monitor ! RequestPrintStatus()
      case _ =>
    }
    println(s"1. Get Experiment Info\n" +
      s"Input : \n")
    val myChoise = io.StdIn.readInt()
    Menu(myChoise)
  }
}
