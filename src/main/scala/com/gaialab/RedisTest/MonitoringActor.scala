package com.gaialab.RedisTest

import akka.actor.{ActorLogging, Actor}
import org.joda.time.DateTime

import scala.collection.mutable.ListBuffer

/**
  * Created by ktz on 16. 1. 23.
  */
case class RequestDone(time : DateTime, duration : Long)
case class RequestPrintStatus()
case class ErrorReport(e : Throwable)
class MonitoringActor extends Actor with ActorLogging{
  var nOfCount : BigInt = 0
  var totalDuration : BigInt = 0

  val mTimeoutList : ListBuffer[RequestDone] = ListBuffer.empty
  val mErrorList : ListBuffer[Throwable] = ListBuffer.empty

  def receive = {
    case RequestDone(time, duration) =>
      if(duration >= 5000)
        mTimeoutList += RequestDone(time, duration)
      nOfCount += 1
      totalDuration += duration
    case ErrorReport(e) =>
      mErrorList += e
    case RequestPrintStatus() =>
      log.info(s"\n" +
        s"Try Count $nOfCount\n" +
        s"Duration Average : ${if(nOfCount != 0) totalDuration / nOfCount else 0}\n" +
        s"TimeOut Count : ${mTimeoutList.size}\n" +
        s"Error Count : ${mErrorList.size}")
  }
}
