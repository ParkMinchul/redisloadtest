package com.gaialab.RedisTest

import java.util.concurrent.{LinkedBlockingQueue, TimeUnit, ThreadPoolExecutor}

import akka.actor.{ActorRef, ActorLogging, Actor}
import org.joda.time.DateTime

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Random, Failure, Success}
import scala.concurrent.duration._

/**
  * Created by ktz on 16. 1. 23.
  */
trait Request
case class RequestGetKey(key : String) extends Request
case class RequestSetKey(key : String) extends Request
case class RequestDeleteKey(key : String) extends Request
class RedisClientPoolActor(host : String, port : Int, password : String = "", nOfKey : Int, monitoringActor: ActorRef) extends RedisClientPoolWrapper with Actor with ActorLogging{
  val mRedisClientPool = MakeRedisClientPool(host, password = password)
  0 until nOfKey foreach{i =>
    self ! RequestSetKey(s"ejabberd:sm:$i@localhost")
    context.system.scheduler.schedule(Random.nextInt(170) + 10 second, 150 second, self, RequestGetKey(s"ejabberd:sm:$i@localhost"))(context.system.dispatcher)
  }

  def receive = {
    case request : Request =>
      val current = System.currentTimeMillis()
      Future {
        request match {
          case RequestGetKey(key) =>
            GetKey(mRedisClientPool, key).unsafePerformIO()
            true
          case RequestSetKey(key) =>
            SetKey(mRedisClientPool, key).unsafePerformIO()
            false
          case RequestDeleteKey(key) =>
            DeleteKey(mRedisClientPool, key).unsafePerformIO()
            false
        }
      }(RedisClientPoolActor.cachedThreadPool).onComplete {
        case Success(b) => if (b && System.currentTimeMillis() - current >= 5 * 1000) log.error("Over 5 Second")
          if(b) monitoringActor ! RequestDone(DateTime.now(), System.currentTimeMillis() - current)
        case Failure(e) => log.error(e, "Failure!")
          monitoringActor ! ErrorReport(e)
      }(RedisClientPoolActor.cachedThreadPool)
  }
}

object RedisClientPoolActor {
  implicit val cachedThreadPool : ExecutionContext =
    ExecutionContext.fromExecutor(new ThreadPoolExecutor(1000, Integer.MAX_VALUE, 60 , TimeUnit.SECONDS, new LinkedBlockingQueue[Runnable]))
}