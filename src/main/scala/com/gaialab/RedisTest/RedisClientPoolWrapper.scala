package com.gaialab.RedisTest

import com.redis.RedisClientPool

import scalaz.effect.IO

/**
  * Created by ktz on 16. 1. 23.
  */
trait RedisClientPoolWrapper {
  def MakeRedisClientPool(host : String, port : Int = 6379, password : String = "") : RedisClientPool =
    new RedisClientPool(host, port, 60000, secret = if(password.isEmpty) None else Some(password))

  def SetKey(redisClientPool: RedisClientPool, key : String) : IO[Boolean] = IO(redisClientPool.withClient
  (client => client.hset(key, "ejabberd@localhost", "asdadoijqoiwdjqowidjoiqjdoiqwjdoisajsjfudguyqwguyqdguyagscjsbcjbsciugwqhidhwqiudhqiudhiuhqiudwhiuwdhqiudhiuwqasdadoijqoiwdjqowidjoiqjdoiqwjdoisajsjfudguyqwguyqdguyagscjsbcjbsciugwqhidhwqiudhqiudhiuhqiudwhiuwdhqiudhiuwq")))

  def GetKey(redisClientPool: RedisClientPool, key : String) : IO[Unit] =
    IO(redisClientPool.withClient(client => client.hget(key, "ejabberd@localhost")))

  def DeleteKey(redisClientPool: RedisClientPool, key : String) : IO[Unit] =
    IO(redisClientPool.withClient(client => client.del(key)))
}