name := "RedisTest"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {
  val AkkaV = "2.4.1"
  val ScalazV = "7.2.0"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % AkkaV,
    "com.typesafe.akka" %% "akka-slf4j" % AkkaV,
    "org.slf4j" % "slf4j-nop" % "1.7.13",
    "net.debasishg" %% "redisclient" % "3.0",
    "org.scalaz" %% "scalaz-core" % ScalazV,
    "org.scalaz" %% "scalaz-effect" % ScalazV,
    "com.github.nscala-time" %% "nscala-time" % "2.6.0"
  )
}